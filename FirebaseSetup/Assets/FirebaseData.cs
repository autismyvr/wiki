﻿using System;
using UnityEngine;
using Firebase.Database;

[Serializable]
public class PlayerData 
{
    public int sceneDataCounter;
    public int saveLanguageData;
    public int saveCoinsData;
    public int saveScoreData;
    public bool isPressedSound; 
    public bool isPressedMusic; 
}

public class FirebaseData : MonoBehaviour
{
    private const string PLAYER_KEY = "PLAYER_KEY";
    private FirebaseDatabase _database;

    public PlayerData LastPlayerData { get; private set; }
    //public PlayerUpdatedEvent OnPlayerUpdated = new PlayerUpdatedEvent();
    
    // Start is called before the first frame update
    void Start()
    {
        _database = FirebaseDatabase.DefaultInstance;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void SaveData(PlayerData player, string reference)
    {
        if (!player.Equals(LastPlayerData))
        {
            _database.GetReference(PLAYER_KEY).SetRawJsonValueAsync(JsonUtility.ToJson(player));
        }
    }
}
