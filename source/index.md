---
title: Projeto de Pesquisa e Inovação - Treino de Habilidades Sociais, Mestrado PPGEB FGA
date: 2019-03-11 12:09:42
---
A KPI Hunters e sua liderença técnica em TI [@Ilton](http://lattes.cnpq.br/4468701388320183) e de sua referência técnica em saúde [@Lídia](http://lattes.cnpq.br/0460105620731292), ambos formados pelo **[Programa de Pós-Graduação em Engenharia Biomédica](https://fga.unb.br/pgengbiomedica)** da [Universidade de Brasília](http://unb.br), [Faculdade Gama (FGA)](http://fga.unb.br), desenvolve Ambientes Imersivos de Realidade Virtual (AIRV) para o treino de habilidades sociais de adolescentes com Transtorno do Espectro Autista (TEA). O GPSS, em parceria com o [Adolescentro, da Secretaria de Saúde do Distrito Federal (SESDF)](http://www.saude.df.gov.br/wp-conteudo/uploads/2018/02/Carta_Servicos-SecSaude-Adolescentro.pdf), desenvolve a intervenção nos adolescentes com TEA aplicando AutismyVR.

## Resultados Preliminares
- Semana 1 (X a Y/0Z/2019):
    - Aceitação de participação na pesquisa:
        - Convites feitos:
        - Aceites recebidos:
        - Taxa de sucesso: %
    - Ganho de engajamento:
        - Quantidade Adesão Intervenção Convencional:
        - Quantidade Adesão Intervenção Com Realidade Virtual:
        - Taxa de sucesso de re-adesão após abandono da intervenção tradicional: 
 - Engajamento:
    - Tecnologia - uso (mais comprometido)
    - Área de Interesse sendo a Tecnologia - desenvolvimento (aspergers)

## Categorias
A equipe executa atividades relacionadas a estas categorias:

 * **Revisão Bibliográfica**: refere-se ao estudo e à documentação dos achados científicos quanto às demandas físicas, psíquicas e psicológicas da pessoa com TEA, bem como o estado da arte da aplicação tecnológica para estes;
 * **Intervenção Terapêutica**: refere-se à aplicação dos produtos desenvolvidos no público com TEA do Adolescentro, bem como a coleta de dados e de evidências;
 * **Modelagem de AIRV**: refere-se ao desenho (2D), aquisição/modelagem de assets (objetos 3D) e modelagem do cenário, bem como a disposição dos assets no cenário modelado;
 * **Animação**: refere-se à animação dos Assets sendo eles referentes ao Jogador/Paciente ou não. Motion (movimentação de membros); Faces (expressões faciais) & Objetos não-humanoides;
 * **Desenvolvimento da Interação**: refere-se à modelagem (diagramação e/ou documentação) e ao desenvolvimento (codificação) das interações do Paciente Imerso (PI);
 * **Desenvolvimento da Inteligência Artificial (IA)**: refere-se à modelagem (diagramação/documentação) e ao desenvolvimento (codificação) de dois grupos de IA: 1) Non-Player Character (NPC), sendo estes Agentes Inteligentes capazes de interagir com o PI e de emular situações de interação social e 2) Agente de adaptação dos requisitos não funcionais às necessidades do PI;   
 * **Ferramentas**: estudos e uso de ferramentas necessárias para executar as demais categorias que se tratam de frentes de trabalho.

 Todas as atividades executadas podem ser acompanhadas pelo [quadro de atividades no Trello](https://trello.com/b/SFqF8OKP/rv-desenvolvimento).


## Overview das Ferramentas
A Stack de Ferramentas utilizadas pela equipe, divididas por frente de trabalho, são:
* **Gerenciamento do Projeto:**
   - [Trello](https://trello.com): Kanban simplificado com as atividades de execução semanal (desenvolvimento e evolução de prontuário das intervenções);
   - [Google Agenda](https://www.google.com/intl/pt-BR/calendar/about/): marcação das datas e horários das intervenções com os pacientes e alocação de espaço no Adolescentro.
* **Produção Científica:**
   - [Google docs](https://www.google.com/docs/about/): Para produção de publicações científicas compartilhada;
   - [Mendeley](https://www.mendeley.com/): Para compartilhamento de referências bibliográficas.
* **Análise de dados:**
   - [Google Data Studio](https://datastudio.google.com/u/0/): Para visualização dos dados capturados pelos sensores de RV e dados da evolução dos prontuários;
   - [Google Sheets](https://www.google.com/docs/about/): Para manuseio de tabelas e trabalho coletivo de normalização e identificação de dados;
   - [Python 3](https://www.python.org/downloads/): A Unity possui integração com o framework de Aprendizado de Máquina (Machine Learning - ML) selecionado: [Tensorflow](https://www.tensorflow.org/), assim sendo, é possível exportar os dados e manuseá-los usando Python e o [Jupyter](https://jupyter.org/).
* **Armazenamento de Dados e Multiplayer (backend):**
   - [Firebase](https://firebase.google.com/): É um Back-end as a Service (BaaS) com sincronização em tempo-real do banco de dados, assim sendo é perfeito para mutliplayer, além de ser altamente escalável. Possui um [SDK específico para Unity](https://firebase.google.com/docs/unity/setup?hl=pt-br), bem como toda a documentação oficial para integração.
* **Desenvolvimento:**
   - [Unity](https://unity.com/pt): É o ambiente de desenvolvimento e modelagem dos AIRV, bem como a engine (motor de física e interação) que exporta os executáveis;
   - [Jetbrains Rider](https://www.jetbrains.com/rider/) & [Visual Studio](https://visualstudio.microsoft.com/): IDEs utilizadas para a codificação dos scripts de interação e personalização do AIRV.     
* **Modelagem:**
   - [Reallusion Character Creator](https://www.reallusion.com/character-creator/): É o ambiente de modelagem de avatares (personagens), bem como personalização de roupas;
   - [Reallusion iClone](https://www.reallusion.com/iclone/): É o ambiente utilizado para a criação das animações dos personagens, desde movimentos de membros à expressões faciais;   
   - [Unity Asset Store](https://unity.com/pt), [Reallusion Marketplace (**comunidade**)](https://marketplace.reallusion.com/) & [Reallusion Content Store (**oficial**)](https://www.reallusion.com/contentstore/): São as lojas nas quais buscamos, adquirimos e/ou pegamos gratuitamente assets para o desenvolvimento;
   - [Acesse a Sessão Assets](/wiki/assets) para maiores informações sobre os assets utilizados neste projeto.

# Firebase

<a href="https://www.youtube.com/watch?v=M2H1VRqSYF4" target="_blank">
<img src="https://i.ytimg.com/vi/M2H1VRqSYF4/hqdefault.jpg?sqp=-oaymwEZCNACELwBSFXyq4qpAwsIARUAAIhCGAFwAQ==&rs=AOn4CLA8z4RZOP553WC5cPTsdQ6oPJ1aaQ" alt="Firebase for Games - Introdução dos benefícios desta plataforma para games"/>
</a>

## Termos de Uso e Licenças
O produto final deste trabalho estará disponível na loja da Apple ([<i class="fab fa-apple"></i> AppStore]()), bem como na [<i class="fab fa-google-play"></i> Google Play](), ambos gratuitamente para download. Porém, por se tratar de um projeto que contempla Assets pagos, estes não serão disponibilizados publicamente, porém  o código-fonte, bem como a documentação de quais Assets foram usados e como utilizá-los para reproduzir esta pesquisa estão disponíveis na [Categoria Assets](/wiki/assets) desta wiki. Além das versões finais nas lojas de apps, será disponibilizado gratuitamente o download dos códigos e Assets gratuitos através da [Unity Assets Store <img id="unityLogo" src="https://logos-download.com/wp-content/uploads/2016/09/Unity_logo_logotype_Unity_3D.png" />]().

[<img class="btn" src="/wiki/images/btn-appstore.png"/>]() [<img class="btn" src="/wiki/images/btn-gplay.png"/>]() [<img class="btn" id="unity" src="/wiki/images/unity-assets-store-btn.png"/>]()
