---
title: Categorização das Ferramentas
date: 2019-08-04 09:17:21
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

category:
 - [Ferramentas, Categorização & Overview]

tags:
---

As ferramentas usadas na produção/desenvolvimento de jogos, animações cinematográficas, realidade virtual e outras artes gráficas digitais se dividem em duas grandes categorias: AAA e Indie. A categoria Indie, quando se referindo a Assets pode também ser cahamada de StarterKit, apesar do termo StarterKit ser mais utilizado para Assets fornecidos pelas próprias ferramentas para engajar desenvolvedores e produtores de conteúdo a começarem a usá-las ao invés de suas concorrentes.

As categorias de ferramentas são:
- __AAA__: utiliza-se da riqueza de detalhes com objetos 3D com maior quantidade de polígonos e texturas em HD.
    - Maior qualidade gráfica;
    - Maior sensação de realismo;
    - Requer maior poder computacional;
    - É Fotorrealista (um genero de arte no qual tenta-se aproximar uma reprodução artística (digital, pintura, desenho) de uma foto de algo real).
    - Em geral são Assets pagos e/ou feitos especificamente para uma única aplicação (artista contratado com exclusividade).
- __StarterKit__: utiliza-se de objetos 3D com menos polígonos e em geral são texturas com qualidade razoável. 
    - Qualidade gráfica aceitável (passa desapercebida quando a jogabilidade é engajadora ou o propósito não é o fotorrealismo);
    - Em geral, devido a baixa quantidade de polígonos e à resolução da textura, requer menor poder computacional;
    - Apesar de ser chamado de "starter" (iniciante), aplicações profissionais se utilizam desta categoria de Assets por diversos motivos:
        - Maior compatibilidade (executável em dispositivos mais antigos e/ou com menor poder computacional (fracos));
        - Menor latência (quando entretenimento é o objetivo, o tempo de resposta e a jogabilidade são mais importantes do que o gráfico).

# Ferramentas de Desenvolvimento
- **Unity** (freemium): Engine de Games (física mecânica, física de iluminação, cameras e etc.); Framework de Desenvolvimento p/ integração do código aos Assets; 
- **Rider (JetBrains)** (pago): IDE com suporte à Markdown para documentação, Modo Debug para encontrar possíveis erros nos códigos, controle/integração com a Unity para facilitar os testes dos códigos com feedbacks visuais; 
- **VisualStudio (Microsoft)** (freemium): IDE como a Rider, porém sem suporte para documentar e com Debug menos completo e preciso como o da Rider, por isso não será usada neste trabalho, mas é totalmente compatível;
- **Google Stadia** (freemium): É uma máquina virtual com configuração de computador gamer adicionada de um software (middleware) para que esta máquina seja fornecida como serviço. Assim sendo, mesmo celulares mais fracos podem ter poder computacional de um computador gamer completo, permitindo o uso de um AIRV mais realista; 
- **Firebase** (freemium): Assim como o Stadia, o Firebase está disponível sozinho e/ou através da Google Cloud Platform. Esta é a ferramenta de Backend as a Service na qual os dados coletados por esta pesquisa será persistido (salvo/armazenado). Também é utilizado para fazer a imersão multi-jogador em tempo-real, permitindo os jogadores interagirem entre si no ambiente virtual. 


# Ferramentas de Modelagem
## Cenário
- **Unity Sistema de Terrenos** (grátis):  Ferramenta da própria Unity, dentro do próprio ambiente de desenvolvimento (nativa), para criação de relevos geográficos, construção da paisagem paisagem (grama, flores, árvores e etc);
- **Unity PolyBrush** (grátis): Ferramenta da própria Unity, dentro do próprio ambiente de desenvolvimento, porém que necessita download pela AssetStore (não-nativa). Esta ferramenta é utilizada principalmente para modelagem de objetos que compõem o cenário, tais como: Arquitetura e Urbanismo (casas, postes e pista).

## Objetos
- **Unity ProBuilder** (grátis): Ferramenta da própria Unity, dentro do próprio ambiente de desenvolvimento, porém que necessita download pela AssetStore (não-nativa). Esta ferramenta é utilizada principalmente para modelagem de objetos acessórios ao cenário, tais como: Móveis (cadeira, mesa, tapete e etc.), Brinquedos (bola, lego e etc.), Comida (hamburguer, pizza e etc.); 
- **Blender** (freemium): Ferramenta externa à Unity usada para modelagem de objetos 3D, desde personagens à objetos de cenários e/ou acessórios. Esta ferramenta foi utilizada somente para converter .obj em .fbx e para arrumar falhas de mapeamento de texturas.
 
## Personagens
- **Reallusion Character Creator** (pago): Ferramenta AAA de modelagem, texturização e vestimenta de personagens capaz de exportar objetos 3D em .FBX compatíveis com a Unity, esta ferramenta é utilizada para aumentar o grau de realismo do AIRV; 
- **AutoDesk CharacterCreator** (grátis): Ferramenta iniciante de modelagem de personagens com menor qualidade gráfica/resolução. Esta é utilizada quando é necessário diminuir a qualidade gráfica para obter melhor desempenho em dispositivos mais fracos.


# Ferramentas de Animação
## Objetos
- **Reallusion iClone** (pago): Ferramenta AAA de animação realista de objetos 3D, tais com veículos;

## Personagens
- **Reallusion iClone** (pago): Ferramenta AAA de animação realista de expressões faciais e movimentos corporais, capaz inclusive de gravar movimentos humanos diversos através de camera e/ou kinect e reproduzí-los nos personagens;
- **AutoDesk Mixamo** (grátis): Ferramenta iniciante de animação muito próxima da realidade com movimento pré-estabelecidos aplicáveis em avatares com estrutura ossea mapeada adequadamente segundo o padrão RigBody. 
