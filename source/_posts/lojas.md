---
title: Lojas de Assets
date: 2019-08-04 08:58:21
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

category:
 - [Ferramentas, Lojas de Assets]

tags:
---

São marketplaces (usuários vendendo para usuários) e/ou lojas oficiais (ferramenta vendendo itens para usuários) das ferramentas de Desenvolvimento e/ou Modelagem. Estas lojas possuem itens gratuitos e/ou pagos. Por utilizarmos itens pagos e itens gratuitos estes não serão disponibilizados ao público, porém serão mencionados os itens adquiridos e/ou baixados para que este trabalho seja reprodutível. 

# (AAA) Reallusion ContentStore
Esta é a loja oficial (fabricante da ferramenta vendendo itens aos usuários) da Reallusion, desenvolvedora de ferramentas de modelagem, como a Reallusion iClone, e de ferramentas de animação, como a Reallusion CrazyTalk. As ferramentas da Reallusion são **majoritariamente** utilizadas para Modelagem e Animação de **Personagens/Avatares**, é **possível** modelar e animar **props (objetos de cenário)**. Esta loja foi utilizada principalmente para adquirir itens que auxiliem na modelagem dos personagens.

# (AAA) Reallusion Marketplace
Este é o marketplace mantido pela Reallusion, porém os itens são desenvolvidos por usuários e vendidos ou fornecidos gratuitamente para outros usuários. Desenvolvedores independentes podem comercializar seus trabalhos nesta ferramenta oficial. Esta loja foi utilizada principalmente para adquirir acessórios de roupas para os personagens. 

# (Starter) Unity AssetStore
Esta é a loja oficial e também o marketplace da Unity. Sendo a Unity uma plataforma de desenvolvimento de jogos, é possível encontrar desde códigos até frameworks e também assets nesta loja. A própria fabricante (Unity) também disponibiliza diversos recursos AAA para a comunidade gratuitamente. Esta loja foi utilizada principalmente para adquirir objetos para os cenários, bem como scripts de interação e a integração com o GoogleVR.
