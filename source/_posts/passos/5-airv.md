---
title: Passo 5. Construção do AIRV
date: 2019-08-08 09:55:36
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

category:
- [Passo a Passo]

thumbnail: /images/autismyvr-dia.jpg

tags:
---

No âmbito deste projeto há apenas uma cena (game level) contemplando todos os cenários propostos (escola, restaurante e consultório). Este AIRV segue como sendo uma cidade que contempla casas e elementos da rua e os cenários propostos.

<!--more-->

# Atmosfera
<a href="https://assetstore.unity.com/packages/tools/particles-effects/aura-2-volumetric-lighting-fog-137148">A atmosfera</a> é o que acrescenta ao cenário o ar de realismo através da iluminação e efeitos visuais.

![Exemplo de realismo na iluminação da Atmosfera](/wiki/images/aura.jpg)

# Cidade
Este asset <a href="https://assetstore.unity.com/packages/3d/environments/urban/suburb-neighborhood-house-pack-modular-72712">propicia uma cidade inteira inicial como casas, carros, ruas e animações, incluindo dia e noite</a>. Após acrescentar esse asset à cena é necessário criar afastamento entre as casas e expandir ruas para criar os espaços para colocar os demais cenários. Lembrando que o AutismyVR possui apenas 1 cena com vários cenários.

![Cidade Inicial com casas, carro, iluminação pública, vias e jardins](/wiki/images/autismyvr-dia.jpg)

# Escola
Este asset trás uma <a href="https://assetstore.unity.com/packages/templates/packs/snaps-art-hd-school-158106">escola completa com todas as dependências e arquitetura, bem como todos os objetos que se encontram em uma escola (desde cadeiras a banheiro e livros)</a>.

![Escola completa com todos os elementos/objetos](/wiki/images/escola.jpg)

# Restaurante
Este asset trás um <a href="https://assetstore.unity.com/packages/3d/props/interior/caf-interior-103957" target="_blank">restaurante aos moldes de uma cafeteria completa, desde cadeiras a elementos de alimentos e disposição dos elementos em uma diagramação adequada a restaurantes</a>.

![Restaurante completo com todos elementos gráficos](/wiki/images/restaurante.jpg)

# Consultório
Este asset foi desenvolvido tendo como base o <a href="https://assetstore.unity.com/packages/3d/environments/interior-lighting-kit-vr-mobile-standalone-84542" target="_blank">Interior Light Kit</a>. Este para este asset foi desenvolvido também um script de IA de adaptação da iluminação.

![Consultório básico, sem considerar elementos para crianças e adolescentes](/wiki/images/consultorio.jpeg)
