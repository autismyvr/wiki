---
title: Passo 1. Instalação das Ferramentas
date: 2019-08-04 09:17:21
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

category:
 - [Passo a Passo]

thumbnail: /images/unity-hub-install.png

tags:
---

Antes de iniciar o desenvolvimento se faz necessária a preparação do ambiente de desenvolvimento e as condições de evolução tal como a estratégia de versionamento e a devida verificação da integração entre as ferramentas.

<!--more-->

## Unity
A maneira mais eficiente de instalar o Unity é através do UnityHub, isso porque este mecanismo permite gerenciar o versionamento da engine. Apesar da Unity apresentar uma capacidade muito boa de adaptação do projeto às configurações de cada versão da engine, ainda é possível ter problemas de versionamento. Isso porque caso o projeto esteja em uma versão muito antiga e a versão de destino, a instalada na máquina, seja mais nova ou o contrário muitas configurações entraram em conflitos e será necessário refazê-las do zero. A constante atualização da engine também apresenta constante alteração na localização dessas configurações, bem como a depreciação (deprecated) de algumas funcionalidades/configurações. Assim sendo o ideal é utilizar o Unity Hub para evitar estas falhas. 

### Download do Unity Hub
[![UnityHub - Download](/wiki/images/unity-hub-download.png)](https://unity3d.com/pt/get-unity/download)

### Instalação do Unity
Siga os passos de instalação do Unity Hub (é bem rápido e simples, pois este é apenas um gerenciador de download). Após instalado o Unity Hub é necessário instalar o Unity, opte sempre pelas versões LTS ou a versão que seu projeto requisitar. Para o projeto AutismyVR as plataformas testadas foram __iOS__ e __Android__.

![UnityHub - Seleção da versão da Unity com base na versão do projeto](/wiki/images/unity-hub-versão-projeto.png)
![UnityHub - Instalação da Unity (botão add)](/wiki/images/unity-hub-add-unity.png)
![UnityHub - Seleção das plataformas para as quais a Unity baixada será capaz de exportar](/wiki/images/unity-hub-unity-platforms.png)

Após a instalação do Unity é necessário [criar um projeto do zero](/wiki/passos/2-project-config/) e seguir os demais passos para configurar o projeto corretamente, porém, continue as instalações das demais dependências antes disso.

## Instalação da IDE
A Integrated Development Environment (IDE) - Ambiente de Desenvolvimento Integrado é o ambiente de desenvolvimento no qual é feito a edição dos códigos-fonte e que permitem diversas integrações como testes, validações do código e integração com a engine (Unity). Neste sentido existem diversas IDEs e editores de texto (estes são mais simples e apenas editam textos sem validações ou integrações). As principais IDE que possuem integração com a Unity são o Visual Studio da Microsoft e o Rider da Jetbrains.

### Visual Studio - Community
Acesse o <a href="https://visualstudio.microsoft.com/pt-br/downloads/" target="_blank">link de download do Visual Studio</a> e o instale. O Visual Studio é a IDE default de edição de C# da Unity. Este possui uma versão gratuita chamada de Community.

### Jetbrains Rider
Acesse o <a href="https://www.jetbrains.com/pt-br/rider/download/" target="_blank">link de download do Jetbrain Rider</a> e o instale, __caso queira maior personalização (customização)__. O único ponto negativo do Rider é não ter versão gratuita de longo prazo (freemium).

### Seleção da IDE na Unity
![Seleção da IDE na Unity (precisa estar em um projeto aberto).](/wiki/images/unity-select-ide.png)
O vídeo [deste link](https://youtu.be/2CvSoo0hlWI?t=41) demonstrará melhor como selecionar a IDE.

## Firebase

### Ambiente Firebase - Unity
1. Para que o SDK do Firebase funcione corretamente no Unity é necessário instalar o Ruby 2.6.0 ``` rvm install 2.6.0 ```, podendo ser via RVM ou não.
1. Adicionar o ruby ao PATH:
    1. ``` $ sudo nano /etc/paths ```
    1. Adicionar na última linha:  $HOME/.gem/ruby/2.6.0/bin 
1. Instalar o cocoapods no gemset default desta versão: ``` $ rvm use 2.6.0 --default && sudo gem install cocoapods --user-install ```.

__Obs__.: esta versão pode alterar, é importante que fique atento a qual versão o cocoapods pode ter de dependência. 

### Setup Servidor - Firebase 
Após a instalação e a configuração básica da Engine e da IDE, será necessário iniciar o processo de criação do servidor que armazenará os dados a serem coletados, bem como disponibilizar a capacidade de tornar o AIRV em Multiplayer em tempo-real. Para isso, crie sua conta usando seu Gmail no <a href="https://firebase.google.com/" target="_blank">Firebase</a>. Micro passos:
1. Crie um projeto com qualquer nome que lhe convenha;
1. Ative o Google Analytics, pois boa parte dos dados coletados podem ser automáticos através desta ferramenta quando o projeto vá além de ensaios clínicos;
1. Selecione como Localização o país no qual será implantado o projeto;
1. Inicie a configuração do Projeto clicando no ícone da Unity.

![Firebase - Selecione o projeto como sendo Unity, demonstrado em vermelho.](/wiki/images/firebase-inicial.png)

A configuração do projeto Unity no Firebase necessita apenas do nome do aplicativo a ser criado e um bundle/pacote. Em geral, este é um domínio/nome de site (DNS), assim sendo ficará: com.nome-grupo-pesquisa.app-name. Baixe os arquivos de configuração e o SDK do firebase e os guarde para importar em seu projeto Unity.

![Firebase - Exemplo de preenchimento dos dados de seu app.](/wiki/images/firebase-registro.png) 

## IDEs e exportação
Ambas as IDEs de desenvolvimento de aplicativos nativos (Android Studio & XCode) são utilizadas principalmente para a exportação dos aplicativos para as lojas de aplicativos, isto porque o UnityHub já baixa as dependências de desenvolvimento.

### Android Studio
O <a href="https://developer.android.com/studio?hl=pt-br" target="_blank">Android Studio</a> se faz necessária para a instalação e gerenciamento do kit de desenvolvimento (SDK) do Android.

Além de baixar o Android Studio, é necessário instalar as dependências do SDK, como na imagem abaixo. Ao clicar instale a versão mais atual do SDK Platforms e também a versão mais atual do Android API, bem como os itens do SDK Tools verifique.
![Gerenciamento e Instalação do SDK do Android no Android Studio](/wiki/images/AndroidStudio-SDKManager.png)

Após a devida instalação do SDK, será necessário verificar se possui a chave de debug:
```shell script
    $ keytool -list -v -alias androiddebugkey -keystore ~/.android/debug.keystore
```

Caso este comando retorne um erro, será necessário criar uma debug.keystore para o debug de aplicações android, informando seus dados para a chave:
```shell script
    $ cd ~/.android/ && keytool -genkey -v -keystore debug.keystore -storepass android -alias androiddebugkey -keypass android -keyalg RSA -keysize 2048 -validity 10000
```
Mediante o comando acima as credenciais de debug será:  
__Keystore name__: debug.keystore  
__Keystore password__: android  
__Key alias__: androiddebugkey  
__Key password__: android  

Execute o comando para recuperar os dados da debug key e jogue o SHA1 no seu projeto firebase:
```shell script
    $ keytool -list -v -alias androiddebugkey -keystore ~/.android/debug.keystore
```

![Android SHA1 Key](/wiki/images/anroid-sha1-debug.png)

### XCode
O <a href="https://apps.apple.com/br/app/xcode/id497799835?mt=12" target="_blank">XCode</a> se faz necessária para a instalação e gerenciamento do kit de desenvolvimento (SDK) do iOS. Após instalar o XCode será necessário baixar o emulador do iOS.

# [Ir ao passo 2](/wiki/passos/2-project-config/)
