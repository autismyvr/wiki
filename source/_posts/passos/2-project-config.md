---
title: Passo 2. Criação e Configuração do Projeto
date: 2019-08-05 14:03:40
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

category:
 - [Passo a Passo]

thumbnail: /images/unity-settings.png

tags:
---

A configuração do projeto é de suma importância, principalmente dependendo da plataforma na qual será instalado o AIRV, tal como os dispositivos móveis. Sem a correta configuração na iniciação do acarretará retrabalho e o risco de problema na refatoração do projeto, estando este em estágio avançado.

<!--more-->

# Criação do Projeto
Para criar o projeto faça-o através do UnityHub clicando em Projects no menu lateral esquerdo e então clique no botão __"new"__ e selecione a opção "3D With Extras" para ficar mais simples de testar as configurações iniciais. Recomendamos que use o nome AutismyVR, porém não há necessidade caso não queira.

![UnityHub - Iniciação do Projeto](/wiki/images/unity-hub-initial-project.png)

# Configuração (Unity e Projeto)

## Preferências da Unity
Essa é uma configuração geral para a Unity, ou seja, para qualquer projeto feito na Unity. [Este link lhe informará como acessar a preferência da Unity](https://docs.unity3d.com/Manual/Preferences.html). No âmbito deste projeto as configurações necessárias são:

![Seleção da IDE na Unity (precisa estar em um projeto aberto).](/wiki/images/unity-select-ide.png)
O vídeo [deste link](https://youtu.be/2CvSoo0hlWI?t=41) demonstrará melhor como selecionar a IDE.

![Configuração dos SDK mobile (Android [vermelho] & iOS [azul]).](/wiki/images/unity-mobile-config.png)
Caso a sua Unity tenha o ícone de alerta como a imagem acima, significa que está desatualizado, você pode remover o módulo Android e readicionar no UnityHub.Como nas imagens abaixo:

![Unity Hub - Atualizando Módulos (exportadores)](/wiki/images/unity-hub-add-module-2.png)

![Unity Hub: Versão do SDK Android atualizado.](/wiki/images/unity-mobile-config-fixed.png)

## Build Settings
Nesta sessão é necessária escolher uma platform que aceite RV, no contexto do AutismyVR ele foi testado apenas em iOS e Android. Selecione um de destes e clique em "switch platform".

![Unity Build Settings - Switch Platform](/wiki/images/unity-switch-platform.png)

## Player/Project Settings
### Add RV ao projeto
Essa é uma configuração específica para o projeto aberto. As configurações necessárias são:
* Vermelho = nome do grupo de pesquisa, <a href="/wiki/passos/1-install/#Setup-Servidor-Firebase" target="_blank">mesmo usado no Firebase</a>;
* Verde = nome do produto/projeto, <a href="/wiki/passos/1-install/#Setup-Servidor-Firebase" target="_blank">mesmo usado no Firebase</a>;
* Amarelo = versão da aplicação, este aparecerá nas lojas de aplicativos;
* Azul = suporte a RV, marque esta opção.

![Unity: Menu > File > Build Settings > Player Settings.](/wiki/images/unity-proj-settings.png)

### GVR (GoogleVR)
Acesse: Player Settings > Settings for iOS > Resolution and Presentation e altere Auto Rotation para "Landscape Left" ONLY (desabilitar portrait orientations). Caso queira saber mais sobre esta "issue", veja em: https://github.com/googlevr/gvr-unity-sdk/issues/897

__Obs.:__ O AustimyVR utiliza o GoogleVR, assim sendo a configuração de RV (XR Settings) deve ser o Cardboard, esta opção só está disponível para dispositivos móveis (Android e iOS). Assim sendo é necessário selecionar as abas Android e iOS para fazer esta configuração. Caso queira usar a nova pipeline de RV recomendamos que siga as instruções da própria Unity, porém para o AutismyVR é usado este workflow.

![Add Cardboard (GoogleVR)](/wiki/images/unity-add-cardboard.png)

Após adicionar o suporte a RV é necessário que você importe o <a href="/wiki/assets/GoogleVRForUnity_1.200.1.unitypackage" target="_blank"> pacote GVR (Google VR) usado no AutismyVR</a>. Caso queira a versão mais recente do GVR baixe-o nas releases do projeto no github: https://github.com/googlevr/gvr-unity-sdk/releases ou [através deste link de download direto da versão estável](https://firebase.google.com/download/unity). Após o download é necessário importar o package:

![Add o package do GoogleVR (cardboard) ao projeto](/wiki/images/unity-add-package.png)

### Config do RV no projeto
Após configurar as plataformas acima, agora é necessário configurar o projeto em si para receber o RV. Neste contexto é necessário configurar as plataformas para ficarem na orientação landscape left e assim evitar bugs como só 1 olho ou erro no frame que divide os olhos que "encaixem" nos óculos de RV e cardboard.

__Obs.:__ não confie no alerta de que ficará landscape left, selecione como na imagem abaixo.

![Forçar orientação Landscape Left](/wiki/images/unity-landscape-left.png)

# Configuração da Iluminação
Como demonstrado na imagem abaixo, clique no local selecionado em __vermelho__ e depois desmarque a opção demonstrada em __verde__ para que evite a auto geração da iluminação o tempo todo. Para utilizar a Baked Light basta que o check em "Real Time Global Illumination" esteja desativado, item em __azul__.

![Unity Light Settings - backed light](/wiki/images/unity-light.png)

# Add o firebase ao projeto Unity

## Add arquivos de configuração
Os arquivos de configuração são os arquivos que possuem os dados do seu projeto (id, chaves de acesso e outros itens). Assim sendo é importante não versionar estes arquivos caso seja um projeto aberto e caso seja um projeto fechado é importante diferenciar arquivos de desenvolvimento de produção. Após trocar a iluminação de tempo-real por baked light sem Auto Geração das luzes será necessário clicar no Generate Light cada vez que alterar algo, porém isso tornará a aplicação mais leve.

![Firebase arquivos config](/wiki/images/firebase-config-files.png)

## Importar o Firebase SDK
O Software Development Kit (SDK) são os códigos e demais itens necessários para simplificar a integração da aplicação em desenvolvimento com outro sistema, no caso com o backend (firebase). Assim sendo, importe este package através do custom package do Unity. O firebase disponibiliza o sdk em .zip e ao extrair haverá duas versões do SDK para cada versão do dotnet. A Unity só suporta dotnet2 e dotnet4, assim sendo, como o firebase apenas disponibiliza o 3 e o 4, basta ignorar o 3 e importar os itens do 4.

![Alteração do dotnet 2 para o 4 na Unity (MenuBar > File > Build Settings > Player Settings > iOS/Android > Other Settings)](/wiki/images/unity-dotnet-4.png)

## Serviços/Funcionalidades Firebase
O Firebase possui diversos serviços, assim sendo segue o significado de cada um e com o check quais são usados pelo AutismyVR

- [x] __FirebaseAnalytics.unitypackage__: serviços de analítico de uso tais como tempo interagindo, perfil de usuário e etc.;
- [x] __FirebaseAuth.unitypackage__: serviço de autenticação (login) de usuário;
- [x] __FirebaseCrashlytics.unitypackage__: serviço de monitoramento de crashs automático;
- [x] __FirebaseDatabase.unitypackage__: banco de dados em tempo-real de baixa latência para aplicativos móveis que exigem estados sincronizados entre dispositivos;
- [x] __FirebaseFirestore.unitypackage__: é um banco de dados com foco na escala e modelo de dados mais intuitivo. Têm consultas mais avançadas e rápidas;
- [ ] __FirebaseDynamicLinks.unitypackage__: serve para transformar páginas do app em links específicos como páginas de sites;
- [ ] __FirebaseFunctions.unitypackage__: são triggers e/ou endpoints REST;
- [ ] __FirebaseInstanceId.unitypackage__: UUID, são identificadores únicos principalmente para FCM (push notification);
- [ ] __FirebaseInvites.unitypackage__: sistema de convite com sistema de associação de usuários como sistemas de marketing multinível (MMN);
- [ ] __FirebaseMessaging.unitypackage__: é o sistema de envio de mensagens (push notifications);
- [ ] __FirebaseRemoteConfig.unitypackage__: é um sistema de configuração remota, permite alterações no cliente partindo de um trigger no servidor;
- [ ] __FirebaseStorage.unitypackage__: é o serviço de upload e armazenamento de arquivos.

<a href="https://firebase.google.com/docs/database/rtdb-vs-firestore?hl=pt" target="_blank">Link para tomada de decisão sobre o BD</a>.

### FirebaseAnalytics
Este serviço diminui a complexidade de coleta de dados do AutismyVR.
![Firebase Analytics](/wiki/images/firebase-analytics.jpg)

### FirebaseAuth
Este serviço serve ao AutismyVR na identificação dos usuários do sistema, bem como associar dados ao usuário.
![Firebase Auth](/wiki/images/firebase-auth.jpg)

### FirebaseCrashlytics
Este serviço é utilizado no AutismyVR para auxiliar na detecção de Crashs/bugs/falhas.
![Firebase Crashlytics](/wiki/images/firebase-crashlytics.jpg)

### FirebaseDatabase
Este serviço é utilizado no AutismyVR para sincronizar a parte multiplayer.
![Firebase Database](/wiki/images/firebase-database-logo.jpg)

### FirebaseFirestore
Este serviço é utilizado no AutismyVR para persistir os dados coletados.
![Firebase Firestore](/wiki/images/firebase-firestore.jpg)

<a href="https://www.youtube.com/watch?v=A6du3DUTIPI" target="_blank">Caso queira maiores explicações, podes acompanhar neste vídeo do YouTube></a>

# [Ir ao passo 3](/wiki/passos/3-data-server/)
