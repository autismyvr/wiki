---
title: Passo 3. Coleta e Armazenamento de Dados
date: 2019-08-06 08:12:36
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

category:
 - [Passo a Passo]

thumbnail: /images/firebase-database.png

---

O AutismyVR se trata de um AIRV com GamePlay MultiPlayers (no qual duas ou mais pessoas são imersas em tempo-real) com coleta de dados automatizada através de sensores de RV e dados de comportamento no AIRV.

<!--more-->

# Imersão Multiplayer
Mediante o conceito de MultiPlayer, no qual vários jogadores são imersos no mesmo jogo/AIRV e interagem uns com os outros em tempo-real, faz-se necessário o desenvolvimento e manutenção de uma aplicação servidora para que haja troca de dados sobre interações entre os dispositivos de cada jogador imerso para que estes dispositivos expressem as ações tomadas no outro dispositivo e as sincronizem.

## Unity Sync MultiPlayer
![Unity Sync Multiplayer Data](/wiki/images/unity-sync-data-sample.gif)

## O servidor
Assim sendo, foi escolhida uma ferramenta Backend as a Service (BaaS) chamada Firebase do Google. Ao utilizar este mecanismo toda a demanda de servidor desde o desenvolvimento da aplicação servidora e configuração da máquina à manutenção é abstraída e tem sua manutenção por parte da plataforma do Google.

<a href="/wiki/passos/1-install/#Setup-Servidor-Firebase" target="_blank">A configuração do projeto no Firebase foi descrito no passo 1</a>.

### Firebase Sync Dados (Multiplayer)
![Sincronização de Dados para o MultiPlayer utilizando o Firebase Database](/wiki/images/firebase-sync-data.gif)

### Firebase Sync Dados (Inserção BD)
![Inserção de dados no Firebase Database com sincronização automática](/wiki/images/firebase-database-sync-sample.gif)

## Integrações com o banco de dados
Para a inserção de dados, como exemplificado acima, no Unity é utillizado o FirebaseDatabase real-time, este dado será utilizado para sincronizar as interações nos dispositivos. Assim sindo, importe o __FirebaseDatabase.unitypackage__ e o __FirebaseFirestore.unitypackage__.

![Import FirebaseDatabase.package no Unity para a persistência](/wiki/images/unity-import-firebase-database.png)

Após a importação dos pacotes de integração com a base de dados é necessário configurar o projeto para que o pacote consiga direcionar os dados para o projeto correto no Firebase, bem como tenha permissões necessárias para tal escrita de dados. <a href="/wiki/passos/2-project-config/#Add-o-firebase-ao-projeto-Unity" target="_blank">Para isso confirme esta etapa</a>. Com os arquivos devidamente alocados no projeto Unity, é necessário criar o código-fonte de inicialização do Firebase.

```csharp FirebaseInit.cs
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Analytics;

public class FirebaseInit : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available) {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                //   app = Firebase.FirebaseApp.DefaultInstance;

                FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
                Debug.Log("Firebase ENABLED (TRUE)");

                // Set a flag here to indicate whether Firebase is ready to use by your app.
            } else {
                UnityEngine.Debug.LogError(System.String.Format(
                    "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
                Debug.Log("Firebase DISABLED (FALSE)");
            }
        });
    }

    // Update is called once per frame
    void Update()
    {

    }
}
```

![Firebase Analytics Setup](/wiki/images/unity-cs-firebase-analytics-active.png)

# Coleta de Dados
 Este servidor pode ser reutilizado para que além de prover o MultiPlayer GamePlay armazene os dados coletados ao longo do uso para posterior análise científica destes dados. Os dados analíticos de uso já são capturados no passo anterior, outros dados como sensores de RV e/ou outros sensores como os de coleta de biodados. A estrutra de dados do Firease se dá através de dois tipos: __Object/Document__ ou  __Collections__, assim sendo o Object/Document serve para registrar elementos que serão acessívels somente através de seus IDs de forma direta e que seus dados são únicos enquanto as Collections armazenam diversos registros e pode ser iterada (é possível navegar e listar seus itens). No contexto do AutismyVR os daados de apoio ao MultiPlayer para imergir mais de uma pessoa é no formato Object e os dados de séries de uso são Collections, porém cada pesquisa deve ter seu próprio roll de dados a serem coletados. O ID dos objects são os IDs dos usuários após a tela de LogIn (ID de Autenticação de sessão de usuário).

 ```csharp FirebaseData.cs
using System;
using UnityEngine;
using Firebase.Database;

[Serializable]
public class PlayerData
{
    public int sceneDataCounter;
    public int saveLanguageData;
    public int saveCoinsData;
    public int saveScoreData;
    public bool isPressedSound;
    public bool isPressedMusic;
}

public class FirebaseData : MonoBehaviour
{
    private const string PLAYER_KEY = "PLAYER_KEY";
    private FirebaseDatabase _database;

    public PlayerData LastPlayerData { get; private set; }
    //public PlayerUpdatedEvent OnPlayerUpdated = new PlayerUpdatedEvent();

    // Start is called before the first frame update
    void Start()
    {
        _database = FirebaseDatabase.DefaultInstance;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SaveData(PlayerData player, string reference)
    {
        if (!player.Equals(LastPlayerData))
        {
            _database.GetReference(PLAYER_KEY).SetRawJsonValueAsync(JsonUtility.ToJson(player));
        }
    }
}

 ```

# [Ir ao passo 4](/wiki/passos/4-ui/)
