---
title: Passo 4. Interface do Usuário
date: 2019-08-07 09:07:53
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

category:
 - [Passo a Passo]

thumbnail: images/unity-ui.png
---

No âmbito do desenvolvimento de Ambientes Imersivos de Realidade Virtual (AIRV), e jogos em geral, há uma clara distinção entre os elementos do jogo (ambiente) e a Interface de Usuário (UI). No âmbito de AIRV (jogos 3D) os elementos do jogo são objetos gráficos 3D e a UI elementos gráficos 2D desenhados em um canvas. A UI é utilizada para exibir dados do jogo/AIRV como por exemplo mapa, vida, etc ou ainda ser o mecanismo de interação com o jogo/AIRV como é o caso do joystick virtual. No caso de AIRV a imersão impossibilita o usuário de utilizar a UI como forma de interação, restringindo esta a exibição de dados ou interação pré-imersão.

<!--more-->

No contexto do projeto AutismyVR e no contexto geral de Ambientes Imersivos de Realidade Virtual (AIRV) a Interface de Usuário é feita através de periféricos e é necessário evitar elementos gráficos que não pertençam ao AIRV. O uso de elementos gráficos na interface podem poluir a visão do usuário trazendo desconforto, principalmente para o público TEA. No caso do AutismyVR a interface de interação do usuário é através de joysticks (controles de vídeo games). A única interface de usuário com elementos gráficos na tela é a tela de LogIn na qual é possível fazer o login via OAuth do Google para que seja possível usar a sessão de usuário para a filtrar, manipular e organizar os dados no banco de dados.

![Joysticks compatíveis](/wiki/images/joysticks.png)

## Configuração do Joystick

Para a integração do Joysctick físico ao AIRV é necessário configurar o script FirstPersonController do StandardAssets:

A = Joystick 1 button 0
B = Joystick 1 button 1
X = Joystick 1 button 2
Y = Joystick 1 button 3

LB = Joystick 1 button 4
RB = Joystick 1 button 5
LT = Joystick 1 button 6
RT = Joystick 1 button 7
L3 = Joystick 1 button 8
R3 = Joystick 1 button 9

START = Joystick 1 button 10
BACK = Joystick 1 button 11

Left Analog Stick X = Axis 1
Left Analog Stick Y = Axis 2 (inverted)

Right Analog Stick X = Axis 3
Right Analog Stick Y = Axis 4 (inverted)

Dpad X = Axis 5
Dpad Y = Axis 6 (inverted)

## Assets Usados para a construção das Interações
*Estes objetos não estão no código de exemplo por questões de direitos autorais, uma vez que estes são vendidos pelos desenvolvedores e assim sendo não pode ser disponibilizado.

NPC = Personagens não controlados por jogadores, seja ele Terapeuta ou o Indivíduo da Intervenção.

1. <a href="https://assetstore.unity.com/packages/tools/ai/behavior-141443" target="_blank">Comportamento dos NPCs (gerar ações aos objetos e personagens do cenário ao ocorrer algum evento gatilho do jogador para tal)</a>
1. <a href="https://assetstore.unity.com/packages/tools/gui/quests-119858" target="_blank">Componente para a criação das atividades a serem cumpridas dentro do AIRV</a>
1. <a href="https://assetstore.unity.com/packages/tools/gui/dialogue-109301" target="_blank">Sistema de comunicação entre o jogador e os NPCs do AIRV</a>
1. <a href="https://assetstore.unity.com/packages/tools/camera/vr-body-79337" target="_blank">Corpo do personagem dos usuários a serem imersos, de forma que o usuário imerso veja este objeto como sendo seu corpo e o movimento cervical implicará ao personagem</a>

# [Ir ao passo 5](/wiki/passos/5-airv/)
