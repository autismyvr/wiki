---
title: Bug Fix e Otimização de Texturas e Materiais
date: 2019-08-04 09:17:21
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

category:
 - [Bug Fix]

---

Ao usar o High Definition Render Pipeline (HDRP) ou o Low Weight Render PipeLine (LWRP), caso objetos/texturas/materiais fiquem rosa, porém funcionam sem estes RP, então é necessário seguir os passos [deste vídeo](https://www.youtube.com/watch?v=HgZHwfF5SuM&t=194s):
    1. Na unity: 
        1. Window > Package Manager > LightWeight Render (instalar ou atualizar) 
        1. Edit > Render Pipeline > Upgrade Project Materials to LightWeight Materials  
