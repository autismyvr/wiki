---
title: Config Rápida e Bug Fix
date: 2019-08-04 09:17:21
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

category:
 - [Bug Fix]

---

A configuração rápida deste projeto se dá considerando as diferenças de sistemas, isso porque algumas configurações default da Unity variam de sistema para sistema (MacOS, Windows e Linux).

<!-- more -->

1. Iniciar o Projeto como 3D sem extras;
1. Fazer o switch platform: File > Build Settings > Switch Platform (escolher a que irá usar);
1. Acessar o Player/Project Settings:
  1. Verificar se tanto o iOS quanto o Android estão na versão .NET 4.x;
  1. Igualar os dados do projeto (bundle identifier, product name, company name e package name) estão iguais ao firebase;
1. Baixar os arquivos de configuração do firebase (google service json e plist) e colocá-los na raíz do projeto;
1. Import o pacote firebase desejado (no start usamos apenas o analytics, mas pode colocar quantos quiser) através do: Assets > Import Package > Custom Package. Navegue até a pasta firebase_unity_sdk/dotnet4. Faça o processo de importação até ter certeza de que importou tudo, pois caso dê algum erro a Unity não fará a importação. __Obs.: a Unity demora a importar, não mexa em anda até que ela termine a importação por completo__;
1. TESTE1: Selecionar o código da direita (com final "scopedRegistries"), o da esquerda é sem isso --> DEU ERRADO;
1. <a href="/wiki/codes/FirebaseInit.cs" target="_blank">Adicionar o código de inicialização do firebase analytics ao projeto</a>;
1. Criar um objeto vazio (empty object) na cena (hierarchy) e acrescentar a este objeto o arquivo de inicialização a este objeto;

ADB CHECK UNITY ERRORS: ´´´ $ adb logcat -s Unity ActivityManager PackageManager dalvikvm DEBUG ´´´

# [Unity 2020 bug fix](https://firebase.googleblog.com/2020/08/firebase-compatibility-with-unity-20201.html) recommended Unity 2019
