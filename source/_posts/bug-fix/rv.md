---
title: Bug Fix e Otimização de Animações
date: 2019-08-04 09:17:21
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

category:
 - [Bug Fix]

---

Com o objetivo de obter o máximo de realismo da Unity, as novas pipelines de renderização foram testadas (HDRP e LWRP), porém o [GoogleVR ainda não é suportado para estas pipelines](https://docs.unity3d.com/Packages/com.unity.render-pipelines.lightweight@5.10/manual/lwrp-builtin-feature-comparison.html). Assim sendo o projeto foi refeito para utilizar o "built-in render pipeline" (o renderizador padrão). 


## Bugs Conhecidos
### Tudo escuro
Acessar Project Settings > Android/iOS > Resolution and Presentation > disable "Start in fullscreen mode"

### Apenas um dos olhos sendo renderizado
Utilizar o "built-in render pipeline" ao invés dos HDRP ou LWRP

## Apenas 1 olho
Acessar Project Settings > Android/iOS > Other Settings > remover Vulkan do Graphics API
1. GVR Emulator (root)
1. Camera RIG (empty obj)
1. main camera filha do camera RIG
1. reticle pointer para a main camera
1. google vr event system no root
1. add script gvr pointer physics  raycaster ao main camera
1. no objeto interativo:
    1. adicionar o componente: EventTrigger e neste adicionar os eventos:
        1. pointer enter
        1. pointer exit
        1. pointer click
        1. adicionar (click no +) de cada evento e arrastar os códigos para dentro do que foi criado (**o código tem que vir do inspetor e não do project**)
