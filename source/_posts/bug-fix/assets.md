---
title: Bug Fix de Assets
date: 2019-08-04 09:17:21
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

category:
 - [Bug Fix]

thumbnail: /images/standard-asset-error.png
---

Com o objetivo de obter a aplicação funcional será necessário resolver problemas (bugs) nos assets importados como o erro no exemplo da thumbnail.

<!--more-->

# Bug Standard Assets
Ao clicar duas vezes no erro no Console da Unity irá abrir o código-fonte aonde está o erro. Neste caso do erro do Standard Asset basta alterar de:
´´´csharp ForcedReset.cs
[RequireComponent(typeof (UI.Text))]
´´´

para:
´´´csharp ForcedReset.cs
using UnityEngine.UI;
[RequireComponent(typeof (Text))]
´´´

e de:
´´´csharp SimpleActivatorMenu.cs
public GUIText camSwitchButton;
´´´

para:
´´´csharp SimpleActivatorMenu.cs
using UnityEngine.UI;
public Text camSwitchButton;
´´´
