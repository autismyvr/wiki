---
title: Bug Fix e Otimização de Animações
date: 2019-08-04 09:17:21
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

category:
 - [Bug Fix]

---

As animações, sejam no iClone ou no Mixamo, podem ser exportadas sem a skin (sem o mesh), porém precisam ser feitas em cima da estrutura óssea do mesh para ser exportada, se não o mesh se perde quando aplica a animação
